#Image ubuntu
FROM ubuntu

MAINTAINER Kevin <rodakevin@live.fr>

RUN apt update

RUN apt -y install wget

RUN apt -y install sudo

RUN apt -y install gnupg

#Installation mongodb

RUN sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 68818C72E52529D4

RUN sudo echo "deb http://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.0.list

RUN sudo apt-get update

RUN sudo apt-get install -y mongodb-org

RUN sudo apt-get -y install mongodb-server

RUN sudo service mongodb start

#Installation nodejs

RUN sudo apt-get install -y nodejs npm

#Installation git

RUN sudo apt install -y git-all

#Clone de l'application et installation

RUN cd var/www && git clone https://gitlab.com/kevinWaouh/jeu-memory.git

RUN cd var/www/jeu-memory && npm install

RUN sudo service mongodb start

EXPOSE 3000 8080
