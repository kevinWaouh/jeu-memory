//On récupère le module Express.
var express = require('express');
var cors = require('cors')
var bodyParser = require('body-parser')


///////////////////////////////////////////////////////
///////// BDD
///////////////////////////////////////////////////////

// La variable mongoose nous permettra d'utiliser les fonctionnalités du module mongoose.
var mongoose = require('mongoose');

//On recupère m'url de notre BDD
var urlmongo = "mongodb://localhost:27017/memory_game";

// On prépare la connexion à la base de donnée
mongoose.connect(urlmongo);

//Création de la base de donnée des scores avec 3 champs : Pseudo, coups et temps
var scores = new mongoose.Schema({
  pseudo : { type : String },
  coups : { type : Number, min : 0 },
  temps : { type : Number, min : 0 },
  casesTrouves : { type : Number, min : 0 },
  difficulte : { type : String, match: /^[a-zA-Z0-9-_]+$/ },
});


//on ce connecte à la base de donnée
var db = mongoose.connection;

//si il y a une erreur lors de la connexion
db.on('error', console.error.bind(console, 'Erreur de connexion'));

//sinon
db.once('open', function (){
  console.log("Connexion à la base de donnée OK");
});


///////////////////////////////////////////////////////
///////// API
///////////////////////////////////////////////////////


// Réglages serveur
var hostname = '0.0.0.0';
var port = 3000;

var app = express();

// permet l'acces aux requêtes provenant d'autres domaines
app.use(cors());

// création du routeur
var myRouter = express.Router();

// middleware
app.use(express.json());
app.use(express.urlencoded());

// je créer le parser application/json
var jsonParser = bodyParser.json()
// je créer le parser application/x-www-form-urlencoded
var urlencodedParser = bodyParser.urlencoded({ extended: false })

// Nous récupèrons le model afin de l'utiliser avec l'api
var tableDesScoresModel = mongoose.model('tableDesScores', scores);

// route de l'api
myRouter.route('/score')

// Ajout des methodes utilisées

// GET
.get(cors(), function(req,res){
  // On recupère les scores dans la BDD en filtrant par score le plus rapide
  tableDesScoresModel.find({}).sort({ casesTrouves: 'desc', temps: 'asc',coups :'asc' }).limit(10).exec(function(err, scores) {
    res.json({scores : scores});
  });

})
//POST
.post( function(req,res){
  // On ajoute l'entrée dans la base de donnée
  // // On crée une instance du Model
  var monScore = new tableDesScoresModel({
    pseudo: req.body.joueur,
    coups: req.body.coups,
    temps: req.body.temps,
    casesTrouves: req.body.casesTrouves,
    difficulte: req.body.difficulte,
  });

  monScore.save(function (err) {
    if (err) {res.json({message : 'error'}); throw err; }
    res.json({message : 'Score ajouté avec succès !'});
  });
})
// put et delete sons inutile dans cette application donc je ne les ai pas mis


// L'application utilise le router
app.use(myRouter);

// Je démarre le serveur
app.listen(port, hostname, function(){
  console.log("serveur démarré");
});
