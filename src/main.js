import Vue from 'vue';

// app est le composant qui gère la totalité du jeu
import App from './App.vue';

// afin de gagner du temps j'ai généré la base de cette
// application à l'aide de vue CLI
// doc => https://cli.vuejs.org/guide/creating-a-project.html#vue-create

// j'enlève productionTip pour ne plus avoir la notification de production au démarrage
Vue.config.productionTip = false;

// j'initialise vue en remplacant l'élement avec l'id app par le composant app
new Vue({
  render: (h) => h(App),
}).$mount('#app');
