# memory_game

Hello word ! Afin de tester mon projet vous avez deux possibilités

## Cloner en local et suivre les instructions ci-dessous
ATTENTION, vous aurez besoin de :

- [mongodb](https://docs.mongodb.com/manual/installation/)
- [nodejs](https://nodejs.org/en/download/)


```
$ git clone https://gitlab.com/kevinWaouh/jeu-memory.git
```
### Étape 1 : installation des fichiers

Aller dans le dossier chemin/vers/jeu-memory puis executez :
```
$ npm install
```

### Étape 2 : puis lancez le serveur grâce à la commande suivante :
```
$ npm run launch
```

## Utiliser docker en utilisant le fichier Dockerfile à la racine du projet

ATTENTION : Je suis novice dans l'utilisation de docker, le projet fonctionne, mais je ne suis pas certain que ce soit la solution la plus optimisée

### Étape 1 : Téléchargez le fichier dans un dossier puis exécutez la commande
```
$ sudo docker build -t kevin/memory .
```

### Étape 2 : lancez le serveur grâce aux commandes suivantes :

```
$ sudo docker run -ti -p 8080:8080 -p 3000:3000 kevin/memory:latest

// Puis une fois en ssh :

$ sudo service mongodb start && cd /var/www/jeu-memory/ && npm run launch

```
